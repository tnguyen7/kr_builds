{% load account %}{% user_display user as user_display %}{% load i18n %}{% autoescape off %}{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}

Hi {{ user_display }} 

Thanks for registering your {{ site_name }} account. To activate your account, please click on the following link.

 {{ activate_url }}


{% endblocktrans %}{% endautoescape %}
{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}
{{ site_domain }}{% endblocktrans %}
