$(document).ready(function() {

    $('#create_build_form').parsley({
      
    });
  
    $(".alert").alert();
    window.setTimeout(function() { $(".alert-success").alert('close'); }, 3000);
    
    // Used to call functions when Edit view loads
    $(function(){
      if($('div.container').is('.show_velk')){
        edit_view_load_velk();

        // Check content checkboxes total
        count_content_checkboxes_edit();
      }
    });

    // Used for create and edit views too...
    // edit_view_load_velk();
  

    // For tooltip ? Runes
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })

    // Gear option 5
    $("#gear_options_num5").change(function() {
      if($("#gear_options_num5").val() != "") {
        // $('#gear_options_stats5').attr('data-parsley-required', 'true');
        $('#gear_options_stats5').attr('required', 'required');    
      } else {
        $('#gear_options_stats5').removeAttr('required');
      }

      $('#create_build_form').parsley().reset();
    });

    $("#gear_options_stats5").change(function() {
      if($("#gear_options_stats5").val() != "") {
        $('#gear_options_num5').attr('required', 'required');  
      } else {
        $('#gear_options_num5').removeAttr('required');
      }

      $('#create_build_form').parsley().reset();
    });

    // Gear option 6
    $("#gear_options_num6").change(function() {
      if($("#gear_options_num6").val() != "") {
        $('#gear_options_stats6').attr('required', 'required');
      } else {
        $('#gear_options_stats6').removeAttr('required');
      }
      $('#create_build_form').parsley().reset();
    });

    $("#gear_options_stats6").change(function() {
      if($("#gear_options_stats6").val() != "") {
        $('#gear_options_num6').attr('required', 'required');
      } else {
        $('#gear_options_num6').removeAttr('required');
      }
      $('#create_build_form').parsley().reset();
    });

    // Gear option 7
    $("#gear_options_num7").change(function() {
      if($("#gear_options_num7").val() != "") {
        $('#gear_options_stats7').attr('required', 'required');
      } else {
        $('#gear_options_stats7').removeAttr('required');
      }
      $('#create_build_form').parsley().reset();
    });

    $("#gear_options_stats7").change(function() {
      if($("#gear_options_stats7").val() != "") {
        $('#gear_options_num7').attr('required', 'required');
      } else {
        $('#gear_options_num7').removeAttr('required');
      }
      $('#create_build_form').parsley().reset();
    });

    // Gear option 8
    $("#gear_options_num8").change(function() {
      if($("#gear_options_num8").val() != "") {
        $('#gear_options_stats8').attr('required', 'required');
      } else {
        $('#gear_options_stats8').removeAttr('required');
      }
      $('#create_build_form').parsley().reset();
    });

    $("#gear_options_stats8").change(function() {
      if($("#gear_options_stats8").val() != "") {
        $('#gear_options_num8').attr('required', 'required');
      } else {
        $('#gear_options_num8').removeAttr('required');
      }
      $('#create_build_form').parsley().reset();
    });

  // Disable content tag checkboxes if more than x = 10
  $('input[name=content_tags]').change(function(event) {    
    enable_disable_content_checkboxes();   
  });

  $('#content_alert_close').on('click', function() {
    $('#content_tags_alert').hide();
  });

  $(".select_all_box").on("click", function() {
    count_content_checkboxes($(this));      
  });

  $('#custom_search_clear').on("click", function() {
    $(".selectpicker").val('default');
    $(".selectpicker").selectpicker("refresh");
  });

  $("#hero_search").keyup(function(){

    // Retrieve the input field text 
    var filter = $(this).val();

    // Loop through the captions div 
    $(".hero_div").each(function(){

     // If the div item does not contain the text phrase fade it out
     if ($(this).attr('hero-name').search(new RegExp(filter, "i")) < 0) {
        $(this).fadeOut();

     // Show the div item if the phrase matches 
     } else {
      $(this).show();
     }
    });

 });

 $("#search_skills").keyup(delay(function(){

  

   $('.hero_div').unmark();

    if ( $(this).val().length == 0 ){
      $('.hero_div').hide();

    } else {
      
      // Search text
      var text = $(this).val();
    
      // Hide all content class element
      $('.hero_div').hide();

      // Search and show
      // $('.hero_div:contains("'+text+'")').show();
      $('.hero_div').filter(function() {
        return RegExp(text, 'gi').test($(this).text());
        // RegExp(text, 'gi').test($(this).text());
        
        
      }).show();
      // $('.hero_div').mark(text);


      if ($('#search_skill_hero_name').prop('checked') ) {
        if ( $('#hero_div_Aisha').prop('visible') ) {
          var div_id = $(".main_hero_div:visible");
          alert(div_id);
          $(div_id).children.show();
        }
      }


      mark_these_words(text);

    }
  
 }, 500));

  $('.search_skill').click(function() {
    var text = $(this).text();
    search_skills_button(text);
  });

  
    
  

   
});

function mark_these_words(text) {
  $('.hero_div:visible').mark(text);  
}

function search_skills_button(keyword) {

  $('.hero_div').unmark();
    
  // Hide all content class element
  $('.hero_div').hide();

  // Search and show
  // $('.hero_div:contains("'+text+'")').show();
  $('.hero_div').filter(function() {
    return RegExp(keyword, 'gi').test($(this).text());
    // RegExp(text, 'gi').test($(this).text());
    
    
  }).show();
  mark_these_words(keyword);
}

function delay(fn, ms) {
  let timer = 0
  return function(...args) {
    clearTimeout(timer)
    timer = setTimeout(fn.bind(this, ...args), ms || 0)
  }
}

function enable_disable_content_checkboxes() {
  if ($('input[name=content_tags]:checked').length >= 20) {
    $("input[name=content_tags]").attr('disabled', 'disabled');
    $("input[name=content_tags]:checked").removeAttr('disabled');
  }
  else {
    $("input[name=content_tags]").removeAttr('disabled');
  }
}

function count_content_checkboxes_edit() {
  var count_unchecked = $(".select_all_box").closest("div").find('input[name=content_tags]:not(:checked)').length;
  var count_checked = $('input[name=content_tags]:checked').length;

  // Disable checkboxes if there are already 10 selected
  if (count_checked >= 20) {
    $(".select_all_box").closest("div").find('input[name=content_tags]:not(:checked)').prop('disabled',true);
  }
}

function count_content_checkboxes(thisObj) {
  var count_unchecked = $(thisObj).closest("div").find('input[name=content_tags]:not(:checked)').length;
  var count_checked = $('input[name=content_tags]:checked').length;

  if ($(thisObj).is(':checked')) {

    if (count_checked + count_unchecked > 20) {
      event.preventDefault();
      $('#content_tags_alert').show();
    } else {
      $(thisObj).closest("div").find(":checkbox").prop('checked',true);
    }

  } 
  else {
    $(thisObj).closest("div").find(":checkbox").prop('checked',false);
    enable_disable_content_checkboxes();
  }
  
}

function edit_view_load_velk() {
  showVelk_w1();
  showVelk_w2();
  showVelk_w3();
  showVelk_a1();
  showVelk_s1();

  update_w1(); // weapon 1
  update_w2();
  update_w3();
  update_a1(); // armor
  update_s1(); // secondary
}

// Validate if total of gear options is 16x
window.Parsley.addValidator('sumof16', {
  validateString: function(value) {
    var sum = 0;
    $(".select_4x").each(function() {
      sum += Number($(this).val().substring(0,1));
    });
    return sum == 16;
  },
  messages: {
    en: 'The total is not 16.',
  }
});

window.Parsley.addValidator('sumof8', {
  validateString: function(value) {
    var sum = 0;
    $(".select_4x").each(function() {
      sum += Number($(this).val().substring(0,1));
    });
    return sum == 16;
  },
  messages: {
    en: 'The total is not 16.',
  }
});

window.Parsley.addValidator('numstatdependency5', {
  validate: function(value) {
        
    return $("#gear_options_num5").val() != "" && $("#gear_options_stats5").val() != "";
  },
  messages: {
    en: 'Either field cannot be empty.',
  }
});

function calculate_gear_total() {
  var sum = 0;
  $(".select_4x").each(function() {
    sum += Number($(this).val().substring(0,1));
  });
  if (sum == 16) {
    return true;
  } else  {
    return false;
  }

}

function showVelk_w1(){ 
  if (document.getElementById('checkbox_runes_weapon1').checked) 
  {
    document.getElementById('runes_weapon1b').style.display ='block';
    document.getElementById('runes_weapon1c').style.display ='block';
    document.getElementById('runes_weapon1a').style.display ='none';
  } else {
    document.getElementById('runes_weapon1b').style.display ='none';
    document.getElementById('runes_weapon1c').style.display ='none';
    document.getElementById('runes_weapon1a').style.display ='block';
  }

}

function showVelk_w2(){ 
  if (document.getElementById('checkbox_runes_weapon2').checked) 
  {
    document.getElementById('runes_weapon2b').style.display ='block';
    document.getElementById('runes_weapon2c').style.display ='block';
    document.getElementById('runes_weapon2a').style.display ='none';
  } else {
    document.getElementById('runes_weapon2b').style.display ='none';
    document.getElementById('runes_weapon2c').style.display ='none';
    document.getElementById('runes_weapon2a').style.display ='block';
  }

}

function showVelk_w3(){ 
  if (document.getElementById('checkbox_runes_weapon3').checked) 
  {
    document.getElementById('runes_weapon3b').style.display ='block';
    document.getElementById('runes_weapon3c').style.display ='block';
    document.getElementById('runes_weapon3a').style.display ='none';
  } else {
    document.getElementById('runes_weapon3b').style.display ='none';
    document.getElementById('runes_weapon3c').style.display ='none';
    document.getElementById('runes_weapon3a').style.display ='block';
  }
}

function showVelk_a1(){ 
  if (document.getElementById('checkbox_runes_armor').checked) 
  {
    document.getElementById('runes_armor1b').style.display ='block';
    document.getElementById('runes_armor1c').style.display ='block';
    document.getElementById('runes_armor1a').style.display ='none';
  } else {
    document.getElementById('runes_armor1b').style.display ='none';
    document.getElementById('runes_armor1c').style.display ='none';
    document.getElementById('runes_armor1a').style.display ='block';
  }
}

function showVelk_s1(){ 
  if (document.getElementById('checkbox_runes_secondary').checked) 
  {
    document.getElementById('runes_secondary1b').style.display ='block';
    document.getElementById('runes_secondary1c').style.display ='block';
    document.getElementById('runes_secondary1a').style.display ='none';
  } else {
    document.getElementById('runes_secondary1b').style.display ='none';
    document.getElementById('runes_secondary1c').style.display ='none';
    document.getElementById('runes_secondary1a').style.display ='block';
  }
}

function update_w1() {
  if (document.getElementById('checkbox_runes_weapon1').checked) {
    $("#runes_weapon1").val($("#runes_weapon1b").val() + " & " + $("#runes_weapon1c").val() );
  }
  else {
    $("#runes_weapon1").val($("#runes_weapon1a").val());
  }
}

function update_w2() {
  if (document.getElementById('checkbox_runes_weapon2').checked) {
    $("#runes_weapon2").val($("#runes_weapon2b").val() + " & " + $("#runes_weapon2c").val() );
  }
  else {
    $("#runes_weapon2").val($("#runes_weapon2a").val());
  }
}

function update_w3() {
  if (document.getElementById('checkbox_runes_weapon3').checked) {
    $("#runes_weapon3").val($("#runes_weapon3b").val() + " & " + $("#runes_weapon3c").val() );
  }
  else {
    $("#runes_weapon3").val($("#runes_weapon3a").val());
  }
}

function update_a1() {
  if (document.getElementById('checkbox_runes_armor').checked) {
    $("#runes_armor").val($("#runes_armor1b").val() + " & " + $("#runes_armor1c").val() );
  }
  else {
    $("#runes_armor").val($("#runes_armor1a").val());
  }
}

function update_s1() {
  if (document.getElementById('checkbox_runes_secondary').checked) {
    $("#runes_secondary").val($("#runes_secondary1b").val() + " & " + $("#runes_secondary1c").val() );
  }
  else {
    $("#runes_secondary").val($("#runes_secondary1a").val());
  }
}
