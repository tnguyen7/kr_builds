$(document).ready(function() {

  $(function(){
    if($('div.container').is('.load_option_remover')){
      remove_dupes();
    }
  });

  // When creating teams, it will remove selected heroes from other dropdowns to prevent duplicates
  $(".option_remover").change(function() {

    $("select.option_remover option[value='" + $(this).data('index') + "']").prop('disabled', false);
    $(this).data('index', this.value);
    $("select.option_remover option[value='" + this.value + "']:not([value=''])").prop('disabled', true);
    $(this).find("option[value='" + this.value + "']:not([value=''])").prop('disabled', false);
    
    $('.option_remover').selectpicker('refresh')
  });

});

function remove_dupes() {
  $('.option_remover').each(function() {
    $("select.option_remover option[value='" + $(this).data('index') + "']").prop('disabled', false);
    $(this).data('index', this.value);
    $("select.option_remover option[value='" + this.value + "']:not([value=''])").prop('disabled', true);
    $(this).find("option[value='" + this.value + "']:not([value=''])").prop('disabled', false);
    
    $('.option_remover').selectpicker('refresh')
  });
}