"""krbuilds URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import RedirectView
from django.contrib.staticfiles.storage import staticfiles_storage

from builds import views
# from filtering import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('builds/', include('builds.urls')),
    path('filtering/', include('filtering.urls')),
    path('top_teams/', include('top_teams.urls')),
    path('skills/', include('skills.urls')),
    path('accounts/dashboard', views.dashboard, name='dashboard'),
    # path('accounts/', include('accounts.urls', namespace='accounts')),
    path('', views.home, name='home'),
    path('new/', views.home_sort_by_new, name='home_sort_by_new'),
    path('terms_of_service', views.terms_of_service, name='terms_of_service'),
    path('privacy_policy', views.privacy_policy, name='privacy_policy'),
    path('about', views.about, name='about'),
    path('faqs', views.faqs, name='faqs'),
    # url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^accounts/', include('allauth.urls')),

    path('ads.txt', RedirectView.as_view(url=staticfiles_storage.url("ads.txt"))),
    
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


