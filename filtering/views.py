from django.shortcuts import render
from django.core.paginator import Paginator
from builds.models import Build, Hero, Content_Tag, HERO_CLASS_CHOICES
from .choices import hero_choices


def select_content(request):
    main_content = Content_Tag.objects.distinct('main_content')
    sub_content = Content_Tag.objects.all().order_by('sub_content')
    
    context = {
        'main_content': main_content,
        'sub_content': sub_content,

    }

    return render(request, 'select_content.html', context)

def select_class(request):
    # builds = Build.objects.filter(content_tags__icontains="")
    
    context = {
        'HERO_CLASS_CHOICES': HERO_CLASS_CHOICES,

    }

    return render(request, 'select_class.html', context)

def filter_atk_type(request, atk_type):
    builds = Build.objects.filter(hero__atk_type=atk_type).order_by('-vote_score')
    context = {
        'builds': builds,

    }
    return render(request, 'filter_content.html', context)

def custom_filter(request):
    # builds = Build.objects.order_by().distinct('hero')
    heroes = Hero.objects.order_by('hero_name')
    main_content = Content_Tag.objects.order_by('main_content').distinct('main_content')
    sub_content = Content_Tag.objects.all()

    context = {
        'heroes': heroes,
        'main_content': main_content,
        'sub_content': sub_content,
        'HERO_CLASS_CHOICES': HERO_CLASS_CHOICES,
    }
    return render(request, 'custom_filter.html', context)

def select_hero(request):
    # builds = Build.objects.filter(content_tags__icontains="")
    
    # heroes = Hero.objects.order_by('hero_name').values_list('hero_name', flat=True).distinct('hero_name')
    builds = Build.objects.order_by('hero').distinct('hero')

    context = {
        'builds': builds,
        # 'class_choices': class_choices,
        'hero_choices': hero_choices,

    }

    return render(request, 'select_hero.html', context)

def filter_custom_search(request):
    queryset_list = Build.objects.order_by('-vote_score')
    if request.method == 'GET':
        keywords = []

        # Hero Class
        if 'multi_filter_class' in request.GET:
            hero_class = request.GET['multi_filter_class']
            if hero_class:
                queryset_list = queryset_list.filter(hero__hero_class__iexact=hero_class)
                keywords.append(hero_class)

        # hero_class = request.GET.get('multi_filter_class') 

        # ATK Type
        if 'multi_filter_atk_type' in request.GET:
            atk_type = request.GET['multi_filter_atk_type']
            if atk_type:
                queryset_list = queryset_list.filter(hero__atk_type__iexact=atk_type)
                keywords.append(atk_type)

        # atk_type = request.GET.get('multi_filter_atk_type') 

        # Heroes
        if 'hero_name' in request.GET:
            hero_name = request.GET['hero_name']
            if hero_name:
                queryset_list = queryset_list.filter(hero__hero_name__iexact=hero_name)
                keywords.append(hero_name)
        
        # if 'hero_name' in request.GET:

        # Content Type - Main Content      

        # Content Type - Sub Content
        if 'sub_content' in request.GET:
            sub_content = request.GET['sub_content']
            if sub_content:
                # m_content = Content_Tag.objects.filter(main_content=main_content).first()
                queryset_list = queryset_list.filter(content_tags__icontains=sub_content)
                keywords.append(sub_content)

        # builds = Build.objects.filter(hero__hero_class=hero_class).filter(hero__atk_type=atk_type).order_by('-vote_score')
        # multi_header = hero_class + " + " + atk_type

        
        paginator = Paginator(queryset_list, 20)
        page = request.GET.get('page')
        builds_page = paginator.get_page(page)

        multi_header = ', '.join(keywords)
        
        context = {
            'builds': builds_page,            
            'multi_header': multi_header,
        }
        return render(request, 'filter_content.html', context)

def content(request, content_value):
    builds = Build.objects.filter(content_tags__icontains=content_value).order_by('-vote_score')
    
    context = {
        'builds': builds,
        'content_type': content_value,
    }
    return render(request, 'filter_content.html', context)

def hero_class(request, hero_class):
    builds = Build.objects.filter(hero__hero_class=hero_class).order_by('-vote_score')
    context = {
        'builds': builds,
        'hero_class': hero_class,
        
    }
    return render(request, 'filter_content.html', context)

def hero_name(request, hero_name):
    builds = Build.objects.filter(hero__hero_name=hero_name).order_by('-vote_score')
    context = {
        'builds': builds,
        'hero_name': hero_name,
        
    }
    return render(request, 'filter_content.html', context)