from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'filtering'

urlpatterns = [
    path('select_content', views.select_content, name='select_content'),
    path('select_class', views.select_class, name='select_class'),
    path('select_hero', views.select_hero, name='select_hero'),
    path('custom_filter', views.custom_filter, name='custom_filter'),
    path('content/<str:content_value>', views.content, name='content'),
    path('hero_class/<str:hero_class>', views.hero_class, name='hero_class'),
    path('atk_type/<str:atk_type>', views.filter_atk_type, name='filter_atk_type'),
    path('hero_name/<str:hero_name>', views.hero_name, name='hero_name'),
    path('multi_filter', views.filter_custom_search, name='filter_custom_search'),
    
]
    
