class_choices = {
    'Knight':'Knight',
    'Warrior':'Warrior',
    'Wizard':'Wizard',
    'Assassin':'Assassin',
    'Archer':'Archer',
    'Mechanic':'Mechanic',
    'Priest':'Priest'
}

hero_choices = {
    'Knight': {
        'Aselica':	'Aselica',
        'Clause':	'Clause',
        'Demia':	'Demia',
        'Dosarta':	'Dosarta',
        'Glenwys':	'Glenwys',
        'Jane':	    'Jane',
        'Loman':	'Loman',
        'Morrah':	'Morrah',
        'Neraxis':	'Neraxis',
        'Phillop':	'Phillop',
        'Ricardo':	'Ricardo',
        'Sonia':	'Sonia',
    },
    'Warrior': {
        'Bernheim':	'Bernheim',
        'Chase':	'Chase',
        'Gau':	    'Gau',
        'Kasel':	'Kasel',
        'Kirze':	'Kirze',
        'Naila':	'Naila',
        'Nicky':	'Nicky',
        'Priscilla':	'Priscilla',
        'Scarlet':	'Scarlet',
        'Seria':	'Seria',
        'Theo':	    'Theo',
        'Viska':	'Viska',
    },
    'Assassin': {
        'Epis':	'Epis',
        'Erze':	'Erze',
        'Ezekiel':	'Ezekiel',
        'Fluss':	'Fluss',
        'Gladi':	'Gladi',
        'Laudia':	'Laudia',
        'Mirianne':	'Mirianne',
        'Nia':	'Nia',
        'Reina':	'Reina',
        'Roi':	'Roi',
        'Tanya':	'Tanya',
    },
    'Archer': {
        'Arch':	'Arch',
        'Dimael':	'Dimael',
        'Luna':	'Luna',
        'Requina':	'Requina',
        'Selene':	'Selene',
        'Shamilla':	'Shamilla',
        'Yanne':	'Yanne',
        'Yuria':	'Yuria',
        'Zafir':	'Zafir',
    }, 
    'Mechanic': {
        'Annette':	'Annette',
        'Cecilia':	'Cecilia',
        'Chrisha':	'Chrisha',
        'Crow':	    'Crow',
        'Hanus':	'Hanus',
        'Kara':	    'Kara',
        'Lakrak':	'Lakrak',
        'Miruru':	'Miruru',
        'Mitra':	'Mitra',
        'Oddy':	    'Oddy',
        'Rodina':	'Rodina',
    }, 
    'Wizard': {
        'Aisha':	'Aisha',
        'Artemia':	'Artemia',
        'Cleo':	    'Cleo',
        'Dakaris':	'Dakaris',
        'Esker':	'Esker',
        'Lewisia':	'Lewisia',
        'Lorraine':	'Lorraine',
        'Maria':	'Maria',
        'Nyx':	    'Nyx',
        'Ophelia':	'Ophelia',
        'Pavel':	'Pavel',
        'Lilia':	'Lilia',
        'Veronica':	'Veronica',
    },
    'Priest': {
        'Baudouin':	'Baudouin',
        'Cassandra':	'Cassandra',
        'Frey':	'Frey',
        'Juno':	'Juno',
        'Kaulah':	'Kaulah',
        'Laias':	'Laias',
        'Lavril':	'Lavril',
        'Leo':	'Leo',
        'Lucias':	'Lucias',
        'May':	'May',
        'Mediana':	'Mediana',
        'Rephy':	'Rephy',
        'Shea':	'Shea',
    }
    

}