from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'top_teams'

urlpatterns = [
    
    path('', views.top_teams, name='top_teams'),
    path('view_teams/<str:content_type>', views.view_teams, name='view_teams'),
    path('create_team/<str:content_value>', views.create_team, name='create_team'),
    path('by_content/', views.by_content, name='by_content'),
    
    
    
    
    

]