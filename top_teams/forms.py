from django import forms
from .models import Team
from builds.choices import content_tags_without_main_tags_choices

class TeamForm(forms.ModelForm):
  
  # content_type = forms.ChoiceField(choices = content_tags_without_main_tags_choices, error_messages={'required': 'This is not a valid content option. (Did you modify the url bar?)'})
  slot1 = forms.CharField(max_length=75)
  slot2 = forms.CharField(max_length=75)
  slot3 = forms.CharField(max_length=75)
  slot4 = forms.CharField(max_length=75)
  slot5 = forms.CharField(max_length=75, required=False)
  slot6 = forms.CharField(max_length=75, required=False)
  slot7 = forms.CharField(max_length=75, required=False)
  slot8 = forms.CharField(max_length=75, required=False)

  class Meta:
    model = Team
    fields = ('slot1', 'slot2', 'slot3', 'slot4', 'slot5', 'slot6', 'slot7', 'slot8')

    

  def clean(self):
    cleaned_data = super().clean()
    slot1 = cleaned_data.get("slot1")
    slot2 = cleaned_data.get("slot2")
    slot3 = cleaned_data.get("slot3")
    slot4 = cleaned_data.get("slot4")
    slot5 = cleaned_data.get("slot5")
    slot6 = cleaned_data.get("slot6")
    slot7 = cleaned_data.get("slot7")
    slot8 = cleaned_data.get("slot8")

    if slot8:
      cond1 = slot1 == slot2 or slot1 == slot3 or slot1 == slot4 or slot1 == slot5 or slot1 == slot6 or slot1 == slot7 or slot1 == slot8
      cond2 = slot2 == slot3 or slot2 == slot4 or slot2 == slot5 or slot2 == slot6 or slot2 == slot7 or slot2 == slot8
      cond3 = slot3 == slot4 or slot3 == slot5 or slot3 == slot6 or slot3 == slot7 or slot3 == slot8
      cond4 = slot4 == slot5 or slot4 == slot6 or slot4 == slot7 or slot4 == slot8
      cond5 = slot5 == slot6 or slot5 == slot7 or slot5 == slot8
      cond6 = slot6 == slot7 or slot6 == slot8
      cond7 = slot7 == slot8
      
      if cond1 or cond2 or cond3 or cond4 or cond5 or cond6 or cond7:
        raise forms.ValidationError("You cannot have the same units on a team.")

    elif slot7:
      cond1 = slot1 == slot2 or slot1 == slot3 or slot1 == slot4 or slot1 == slot5 or slot1 == slot6 or slot1 == slot7 
      cond2 = slot2 == slot3 or slot2 == slot4 or slot2 == slot5 or slot2 == slot6 or slot2 == slot7 
      cond3 = slot3 == slot4 or slot3 == slot5 or slot3 == slot6 or slot3 == slot7
      cond4 = slot4 == slot5 or slot4 == slot6 or slot4 == slot7 
      cond5 = slot5 == slot6 or slot5 == slot7 
      cond6 = slot6 == slot7 

      if cond1 or cond2 or cond3 or cond4 or cond5 or cond6:
        raise forms.ValidationError("You cannot have the same units on a team.")

    elif slot6:
      cond1 = slot1 == slot2 or slot1 == slot3 or slot1 == slot4 or slot1 == slot5 or slot1 == slot6  
      cond2 = slot2 == slot3 or slot2 == slot4 or slot2 == slot5 or slot2 == slot6  
      cond3 = slot3 == slot4 or slot3 == slot5 or slot3 == slot6 
      cond4 = slot4 == slot5 or slot4 == slot6 
      cond5 = slot5 == slot6  

      if cond1 or cond2 or cond3 or cond4 or cond5:
        raise forms.ValidationError("You cannot have the same units on a team.")

    elif slot4:
      cond1 = slot1 == slot2 or slot1 == slot3 or slot1 == slot4  
      cond2 = slot2 == slot3 or slot2 == slot4  
      cond3 = slot3 == slot4 

      if cond1 or cond2 or cond3:
        raise forms.ValidationError("You cannot have the same units on a team.")

