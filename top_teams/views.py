from django.shortcuts import render, redirect, get_object_or_404 
from django.contrib import messages
from builds.choices import content_tags_choices, heroes_list, content_tags_without_main_tags_choices
from builds.models import Hero, Content_Tag
from .forms import TeamForm
from .models import Team
from django import forms
from django.db.models import Count
from django.contrib.auth.decorators import login_required


def top_teams(request):
  main_content = Content_Tag.objects.distinct('main_content')
  sub_content = Content_Tag.objects.all().order_by('sub_content')

  top_teams = Team.objects.all().order_by('-created_at')[:10]
    
  context = {
    'main_content': main_content,
    'sub_content': sub_content,
    'teams': top_teams,

  }

  return render(request, 'top_teams.html', context)

def view_teams(request, content_type):
  main_content = Content_Tag.objects.distinct('main_content')
  sub_content = Content_Tag.objects.all().order_by('sub_content')

  teams = Team.objects.raw('SELECT * FROM (SELECT DISTINCT ON(hash_value) *, COUNT(*) OVER (PARTITION BY hash_value) as total FROM top_teams_team WHERE content_type=%s) t ORDER BY total DESC', [content_type])[:10]
  
  context = {
    'content_type': content_type,
    'teams': teams,
    'main_content': main_content,
    'sub_content': sub_content,

  }
  return render(request, 'view_teams.html', context)

def by_content(request):
  main_content = Content_Tag.objects.distinct('main_content')
  sub_content = Content_Tag.objects.all().order_by('sub_content')

  sub_content = sub_content.exclude(sub_content='Guild War')

  context = {
    'main_content': main_content,
    'sub_content': sub_content,

  }
  return render(request, 'by_content.html', context)

@login_required(login_url='/accounts/login')
def create_team(request, content_value):
  content_tag = get_object_or_404(Content_Tag, sub_content=content_value)
  content_tags_all = Content_Tag.objects.all()
  
  heroes = Hero.objects.order_by('hero_name')

  if request.method == "POST":
    form = TeamForm(request.POST or None)

    slot1 = request.POST['slot1']
    slot2 = request.POST['slot2']
    slot3 = request.POST['slot3']
    slot4 = request.POST['slot4']
    slot5 = ''
    slot6 = ''
    slot7 = ''
    slot8 = ''
    slot9 = ''
    if 'slot5' in request.POST:
      slot5 = request.POST['slot5']
      print(slot5)
    if 'slot6' in request.POST:
      slot6 = request.POST['slot6']
      print(slot6)
    if 'slot7' in request.POST:
      slot7 = request.POST['slot7']
    if 'slot8' in request.POST:
      slot8 = request.POST['slot8']
    if 'slot9' in request.POST:
      slot9 = request.POST['slot9']

    # Create list from data, sort data, convert to string, and get hash value from string
    my_list = []
    my_list.append(slot1)
    my_list.append(slot2)
    my_list.append(slot3)
    my_list.append(slot4)
    if slot5:
      my_list.append(slot5)
    if slot6:
      my_list.append(slot6)
    if slot7:
      my_list.append(slot7)
    if slot8:
      my_list.append(slot8)
    if slot9:
      my_list.append(slot9)
    my_list.sort()
    
    my_string = ''.join(my_list)
    my_hash = hash(my_string)

    # Check if the same user has already submitted the same team before...
    # Compare string to any existing data, if it exists  
    team_exists_same_user = Team.objects.filter(hash_string=my_string).filter(user_id=request.user.id).filter(content_type=content_value).exists()

    if team_exists_same_user:
      messages.warning(request, 'You have already submitted this team lineup.')
    
    if form.is_valid() and not team_exists_same_user:
      team = form.save(commit=False)
      team.user_id = request.user.id
      team.hash_value = my_hash
      team.hash_string = my_string
      team.content_type = content_value

      team.save()
      messages.success(request, 'Team submitted successfully.')
      return redirect('top_teams:view_teams', content_type=content_value)
    else:
      context = {
      'form': form,
      'sub_content': content_value,
      'content_tag': content_tag,
      'heroes_list': heroes_list,
      'heroes': heroes, 
      'content_tags_all': content_tags_all, 

    }
    return render(request, 'create_team.html', context)
  else:
    form = TeamForm()

  context = {
    'form': form,
    'sub_content': content_value,
    'content_tag': content_tag,
    'heroes_list': heroes_list,
    'heroes': heroes,
    'content_tags_all': content_tags_all,
    
  }
  return render(request, 'create_team.html', context)
