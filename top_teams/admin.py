from django.contrib import admin
from .models import Team

class TeamAdmin(admin.ModelAdmin):
  list_display = ('content_type', 'hash_value', 'slot1', 'slot2', 'slot3', 'slot4','created_at')

admin.site.register(Team, TeamAdmin)
