from django.db import models
# from vote.models import VoteModel

from builds.choices import content_tags_without_main_tags_choices

class Team(models.Model):
  content_type = models.CharField(max_length=75)
  slot1 = models.CharField(max_length=50)
  slot2 = models.CharField(max_length=50)
  slot3 = models.CharField(max_length=50)
  slot4 = models.CharField(max_length=50)
  slot5 = models.CharField(max_length=50, blank=True)
  slot6 = models.CharField(max_length=50, blank=True)
  slot7 = models.CharField(max_length=50, blank=True)
  slot8 = models.CharField(max_length=50, blank=True)
  slot9 = models.CharField(max_length=50, blank=True)
  hash_string = models.CharField(max_length=100)
  hash_value = models.CharField(max_length=100)
  # score = models.IntegerField(default=1)
  user_id = models.BigIntegerField()
  created_at = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return self.content_type
