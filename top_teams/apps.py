from django.apps import AppConfig


class TopTeamsConfig(AppConfig):
    name = 'top_teams'
