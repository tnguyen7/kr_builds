# Generated by Django 2.2.3 on 2019-09-18 20:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('top_teams', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='content_type',
            field=models.CharField(choices=[('Chapters 1 to 7', 'Chapters 1 to 7'), ('Chapter 8', 'Chapter 8'), ('Chapter 9', 'Chapter 9'), ('League of Honor', 'League of Honor'), ('League of Victory', 'League of Victory'), ('Guild War', 'Guild War'), ('Fire Dragon', 'Fire Dragon'), ('Ice Dragon', 'Ice Dragon'), ('Poison Dragon', 'Poison Dragon'), ('Black Dragon', 'Black Dragon'), ('Fire Dragon Hard', 'Fire Dragon Hard'), ('Ice Dragon Hard', 'Ice Dragon Hard'), ('Poison Dragon Hard', 'Poison Dragon Hard'), ('Black Dragon Hard', 'Black Dragon Hard'), ('Lava Raid', 'Lava Raid'), ('Hero Suppression Raid', 'Hero Suppression Raid'), ('Hero Protection Raid', 'Hero Protection Raid'), ('Dark Legion Raid', 'Dark Legion Raid'), ('Velkazaar (GC)', 'Velkazaar'), ('Tyrfas (GC)', 'Tyrfas'), ('Lakreil (GC)', 'Lakreil'), ('Protianus', 'Protianus'), ('Xanadus', 'Xanadus'), ('Mountain Fortress', 'Mountain Fortress'), ('Xakios (GRHell)', 'Xakios'), ('Nordik (GRHell)', 'Nordik'), ('Nubis (GRHell)', 'Nubis'), ('Gushak (GRHell)', 'Gushak'), ('Tyrfas (GRHell)', 'Tyrfas'), ('Lakreil (GRHell)', 'Lakreil'), ('Manticore (GRHell)', 'Manticore'), ('Urkak', 'Urkak'), ('Satria', 'Satria'), ('Kerberen', 'Kerberen'), ('Obrigard', 'Obrigard'), ('Sithrael', 'Sithrael'), ('Garrow', 'Garrow'), ('Thorpe', 'Thorpe'), ('Kallax', 'Kallax'), ('ToC: Floor 70', 'ToC: Floor 70'), ('ToC: Floor 71', 'ToC: Floor 71'), ('ToC: Floor 72', 'ToC: Floor 72'), ('ToC: Floor 73', 'ToC: Floor 73'), ('ToC: Floor 74', 'ToC: Floor 74'), ('ToC: Floor 75', 'ToC: Floor 75'), ('ToC: Floor 76', 'ToC: Floor 76'), ('ToC: Floor 77', 'ToC: Floor 77'), ('ToC: Floor 78', 'ToC: Floor 78'), ('ToC: Floor 79', 'ToC: Floor 79'), ('ToC: Floor 80', 'ToC: Floor 80'), ('Labyrinth of Defense (Knight)', 'Labyrinth of Defense (Knight)'), ('Labyrinth of Charge (Warrior)', 'Labyrinth of Charge (Warrior)'), ('Labyrinth of Ambush (Assassin)', 'Labyrinth of Ambush (Assassin)'), ('Labyrinth of Aim (Archer)', 'Labyrinth of Aim (Archer)'), ('Labyrinth of Penetration (Mechanic)', 'Labyrinth of Penetration (Mechanic)'), ('Labyrinth of Explosion (Wizard)', 'Labyrinth of Explosion (Wizard)'), ('Labyrinth of Heal (Priest)', 'Labyrinth of Heal (Priest)')], max_length=75),
        ),
    ]
