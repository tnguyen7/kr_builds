# Generated by Django 2.2.3 on 2019-09-20 19:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('top_teams', '0006_remove_team_score'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='hash_value',
            field=models.CharField(default='def', max_length=100),
            preserve_default=False,
        ),
    ]
