from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'skills'

urlpatterns = [
    path('', views.search_skills, name='search_skills'),

]
    
