from django.shortcuts import render
from builds.models import Skillset

def search_skills(request):
  skillset = Skillset.objects.all().order_by('hero__hero_name')
  context = {
    'skillset': skillset,
  }
  return render(request, 'search_skills.html', context) 
