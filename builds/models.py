from django.db import models
from vote.models import VoteModel
from django.contrib.auth.models import User
import datetime

HERO_CLASS_CHOICES = [
    ('Knight', 'Knight'),
    ('Warrior', 'Warrior'),
    ('Assassin', 'Assassin'),
    ('Archer', 'Archer'),
    ('Mechanic', 'Mechanic'),
    ('Wizard', 'Wizard'),
    ('Priest', 'Priest')
]

ATK_TYPE_CHOICES = [
    ('Physical', 'Physical'),
    ('Magic', 'Magic')
]

POSITION_CHOICES = [
    ('Front', 'Front'),
    ('Middle', 'Middle'),
    ('Back', 'Back')
]

class Hero(models.Model):
    hero_name = models.CharField(max_length=50, unique=True)
    hero_class= models.CharField(max_length=50, choices=HERO_CLASS_CHOICES)
    hero_image = models.ImageField(upload_to='heroes', blank=True)
    atk_type = models.CharField(max_length=25, choices=ATK_TYPE_CHOICES)
    position = models.CharField(max_length=25, choices=POSITION_CHOICES)
    unique_treasure1_name = models.CharField(max_length=100, default='default')
    unique_treasure2_name = models.CharField(max_length=100, default='default')
    unique_treasure3_name = models.CharField(max_length=100, default='default')
    unique_treasure4_name = models.CharField(max_length=100, default='default')
    unique_treasure1_image = models.ImageField(upload_to='unique_treasures', blank=True)
    unique_treasure2_image = models.ImageField(upload_to='unique_treasures', blank=True)
    unique_treasure3_image = models.ImageField(upload_to='unique_treasures', blank=True)
    unique_treasure4_image = models.ImageField(upload_to='unique_treasures', blank=True)
    def __str__(self):
        return self.hero_name

class Skillset(models.Model):
    hero = models.ForeignKey('Hero', on_delete=models.CASCADE)
    skill_1_name = models.CharField(max_length=100, blank=True)
    skill_2_name = models.CharField(max_length=100, blank=True)
    skill_3_name = models.CharField(max_length=100, blank=True)
    skill_4_name = models.CharField(max_length=100, blank=True)
    skill_1_description = models.TextField(blank=True)
    skill_2_description = models.TextField(blank=True)
    skill_3_description = models.TextField(blank=True)
    skill_4_description = models.TextField(blank=True)
    def __str__(self):
        return self.hero.hero_name

class Content_Tag(models.Model):
    main_content = models.CharField(max_length=75)
    sub_content = models.CharField(max_length=75, unique=True)
    units_in_team = models.IntegerField()
    def __str__(self):                                        
        return self.sub_content

class Build(VoteModel, models.Model):
    title = models.CharField(max_length=200)
    # hero_name = models.CharField(max_length=100)
    hero = models.ForeignKey(Hero, on_delete=models.CASCADE)
    content_tags = models.CharField(max_length=500)
    gearset_quantity1 = models.CharField(max_length=5)
    gearset_type1 = models.CharField(max_length=200)
    gearset_quantity2 = models.CharField(max_length=5)
    gearset_type2 = models.CharField(max_length=200)
    gear_options_num1 = models.CharField(max_length=5)
    gear_options_stats1 = models.CharField(max_length=200)
    gear_options_num2 = models.CharField(max_length=5)
    gear_options_stats2 = models.CharField(max_length=200)
    gear_options_num3 = models.CharField(max_length=5)
    gear_options_stats3 = models.CharField(max_length=200)
    gear_options_num4 = models.CharField(max_length=5)
    gear_options_stats4 = models.CharField(max_length=200)
    gear_options_num5 = models.CharField(max_length=5)
    gear_options_stats5 = models.CharField(max_length=200)
    gear_options_num6 = models.CharField(max_length=5)
    gear_options_stats6 = models.CharField(max_length=200)
    gear_options_num7 = models.CharField(max_length=5)
    gear_options_stats7 = models.CharField(max_length=200)
    gear_options_num8 = models.CharField(max_length=5)
    gear_options_stats8 = models.CharField(max_length=200)
    unique_treasure_skill_num = models.CharField(max_length=5)
    unique_treasure_option1 = models.CharField(max_length=100)
    unique_treasure_option2 = models.CharField(max_length=100, blank=True)
    unique_treasure_option3 = models.CharField(max_length=100, blank=True)
    unique_treasure_option4 = models.CharField(max_length=100, blank=True)
    unique_treasure_options_num1 = models.CharField(max_length=10)
    unique_treasure_options_num2 = models.CharField(max_length=10, blank=True)
    unique_treasure_options_num3 = models.CharField(max_length=10, blank=True)
    unique_treasure_options_num4 = models.CharField(max_length=10, blank=True)
    enchant1 = models.CharField(max_length=200)
    enchant2 = models.CharField(max_length=200)
    enchant3 = models.CharField(max_length=200)
    enchant4 = models.CharField(max_length=200)
    runes_weapon1 = models.CharField(max_length=200)
    runes_weapon2 = models.CharField(max_length=200)
    runes_weapon3 = models.CharField(max_length=200)
    runes_armor = models.CharField(max_length=200)
    runes_secondary = models.CharField(max_length=200)
    artifact = models.CharField(max_length=200)
    transcend_perks = models.CharField(max_length=200)
    summary = models.TextField(blank=True)
    accessory_type = models.CharField(max_length=50)
    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.title


