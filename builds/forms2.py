from django import forms
from .models import Build
from django.core.exceptions import ValidationError
from .choices import content_tags_choices



class BuildForm(forms.ModelForm):
    class Meta:
        model = Build
        # exclude = ['created_at', 'updated_at','vote_score', 'num_vote_up', 'num_vote_down']
        
        # content_tags = forms.MultipleChoiceField(choices=content_tags_choices, label="Content Tags", required=True)
        fields = ['title', 'content_tags']
        # fields = []

    # def __init__(self, *args, **kwargs):
        # self.user = kwargs.pop('user',None)
        # super(BuildForm, self).__init__(*args, **kwargs)
        # self.fields['content_tags'].choices = content_tags_choices

        # content_tags = forms.ChoiceField(choices=(), required=True)

        


    # def __init__(self, *args, **kwargs):
    #     super(BuildForm, self).__init__(*args, **kwargs)
    #     self.fields['hero'].widget = forms.RadioSelect

    # def clean(self):
    #     title = self.cleaned_data["title"]
    #     if len(title) < 5:
    #         raise ValidationError('Title needs to be 5 characters or greater.')
    #     if len(title) > 140:
    #         raise ValidationError('Title needs to be 140 characters or less.')
    #     return self.cleaned_data

    def clean_title(self):
        title = self.cleaned_data['title']
        if len(title) < 5:
            raise ValidationError('Title needs to be 5 characters or greater.')
        if len(title) > 140:
            raise ValidationError('Title needs to be 140 characters or less.')
        return title

    def clean_content_tags(self):
        content_tags = self.cleaned_data['content_tags']
        
        if len(content_tags) < 3:
            raise ValidationError('Please select at least 1 content tag.')
        return content_tags

    # def clean_hero(self):
    #     hero_name = self.cleaned_data["heroes"]
    #     print(hero_name)
    #     if hero_name:
    #         hero = Hero.objects.get(hero_name=hero_name)
    #     print(hero)
    #     if not hero:
    #         raise ValidationError('Please select a hero.')
    #     return hero

    # def clean_summary(self):
    #     summary = self.cleaned_data["summary"]
    #     if len(summary) > 3000:
    #         raise ValidationError('Summary cannot exceed 3000 characters.')
    #     return summary

    # def clean(self):
        


    
    #     title = self.cleaned_data.get("title")
    #     hero = self.cleaned_data.get("hero")
    #     content_tags = self.cleaned_data.get("content_tags")
    #     gearset_quantity1 = self.cleaned_data.get("gearset_num1")
    #     gearset_quantity2 = self.cleaned_data.get("gearset_num2")
    #     gearset_type1 = self.cleaned_data.get("dropdown_gearsets1")
    #     gearset_type2 = self.cleaned_data.get("dropdown_gearsets2")
    #     gear_options_num1 = self.cleaned_data.get("gear_options_num1")
    #     gear_options_num2 = self.cleaned_data.get("gear_options_num2")
    #     gear_options_num3 = self.cleaned_data.get("gear_options_num3")
    #     gear_options_num4 = self.cleaned_data.get("gear_options_num4")
    #     gear_options_num5 = self.cleaned_data.get("gear_options_num5")
    #     gear_options_num6 = self.cleaned_data.get("gear_options_num6")
    #     gear_options_num7 = self.cleaned_data.get("gear_options_num7")
    #     gear_options_num8 = self.cleaned_data.get("gear_options_num8")
    #     gear_options_stats1 = self.cleaned_data.get("dropdown_stats1")
    #     gear_options_stats2 = self.cleaned_data.get("dropdown_stats2")
    #     gear_options_stats3 = self.cleaned_data.get("dropdown_stats3")
    #     gear_options_stats4 = self.cleaned_data.get("dropdown_stats4")
    #     gear_options_stats5 = self.cleaned_data.get("dropdown_stats5")
    #     gear_options_stats6 = self.cleaned_data.get("dropdown_stats6")
    #     gear_options_stats7 = self.cleaned_data.get("dropdown_stats7")
    #     gear_options_stats8 = self.cleaned_data.get("dropdown_stats8")
    #     unique_treasure_skill_num = self.cleaned_data.get("unique_treasure")
    #     unique_treasure_option1 = self.cleaned_data.get("unique_treasure_option1")
    #     unique_treasure_option2 = self.cleaned_data.get("unique_treasure_option2")
    #     enchant1 = self.cleaned_data.get("dropdown_enchant1")
    #     enchant2 = self.cleaned_data.get("dropdown_enchant2")
    #     enchant3 = self.cleaned_data.get("dropdown_enchant3")
    #     enchant4 = self.cleaned_data.get("dropdown_enchant4")
    #     runes_weapon1 = self.cleaned_data.get("dropdown_runes_weapon1")
    #     runes_weapon2 = self.cleaned_data.get("dropdown_runes_weapon2")
    #     runes_weapon3 = self.cleaned_data.get("dropdown_runes_weapon3")
    #     runes_armor = self.cleaned_data.get("dropdown_runes_armor")
    #     runes_secondary = self.cleaned_data.get("dropdown_runes_secondary")
    #     artifact = self.cleaned_data.get("dropdown_artifacts")
    #     transcend_perks = self.cleaned_data.get("transcend_perks")
    #     summary = self.cleaned_data.get("summary")
    #     accessory_type = self.cleaned_data.get("accessory_type")
    #     user = self.cleaned_data.get("user")
        

            
            
            
            


            
            
            