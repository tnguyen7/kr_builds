from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.core.exceptions import ValidationError
from django.contrib import messages
from django.core.paginator import Paginator
from .models import Build, Hero, Content_Tag
from .choices import *
from .forms import BuildForm

from django import forms

def upvote(request, build_id):
    if request.method == 'POST':
        build = Build.objects.get(pk=build_id)
        # post.votes_total += 1
        if not build.votes.up(request.user.id):
            build.votes.delete(request.user.id)
        # post.save()
        # return redirect('home')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def downvote(request, build_id):
    if request.method == 'POST':
        build = Build.objects.get(pk=build_id)
        # post.votes_total -= 1
        if not build.votes.down(request.user.id):
            build.votes.delete(request.user.id)
        # post.save()
        # return redirect('home')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def home(request):
    # posts = Post.objects.order_by('-votes_total')
    # builds = Build.objects.all()
    builds = Build.objects.order_by('-vote_score')
    # for build in builds:
    #     build.content_tags = build.content_tags.split(' ')

    paginator = Paginator(builds, 20)
    page = request.GET.get('page')
    builds_page = paginator.get_page(page)

    context = {
        'builds': builds_page,
        
    }

    return render(request, 'home.html', context)

def home_sort_by_new(request):
    builds = Build.objects.order_by('-created_at')
    for build in builds:
        build.content_tags = build.content_tags.split(' ')

    paginator = Paginator(builds, 20)
    page = request.GET.get('page')
    builds_page = paginator.get_page(page)

    context = {
        'builds': builds_page,
        
    }

    return render(request, 'home.html', context)

def view_build(request, build_id):
    # build = Build.objects.filter(id__iexact=build_id)
    build = get_object_or_404(Build, pk=build_id)
    # tags = ''.join(build.content_tags).split('[]')
    tags = build.content_tags.strip('[]').replace("'", "").split(', ')
    
    context = {
        'build': build,
        'tags': tags,
        
    }

    return render(request, 'builds/view_build.html', context)

def dashboard(request):
    user_builds = Build.objects.order_by('-created_at').filter(user_id=request.user.id)

    context = {
        'builds': user_builds
    }

    return render(request, 'accounts/dashboard.html', context)

@login_required
def edit_build(request, build_id):
    build = Build.objects.get(pk=build_id)
    main_content = Content_Tag.objects.distinct('main_content')
    sub_content = Content_Tag.objects.all().order_by('sub_content')

    if '&' in build.runes_weapon1:
        runes_weapon1_velk = build.runes_weapon1.split(' & ')
    else:
        runes_weapon1_velk = ''

    if '&' in build.runes_weapon2:
        runes_weapon2_velk = build.runes_weapon2.split(' & ')
    else:
        runes_weapon2_velk = ''

    if '&' in build.runes_weapon3:
        runes_weapon3_velk = build.runes_weapon3.split(' & ')
    else:
        runes_weapon3_velk = ''

    if '&' in build.runes_armor:
        runes_armor_velk = build.runes_armor.split(' & ')
    else:
        runes_armor_velk = ''

    if '&' in build.runes_secondary:
        runes_secondary_velk = build.runes_secondary.split(' & ')
    else:
        runes_secondary_velk = ''
    

    if request.user == build.user:
         # instance = BuildForm.objects.get(pk=build_id)
        
        form = BuildForm(request.POST or None, instance=build)

        if request.method == "POST" and form.is_valid():
            # build = form.save(commit=False)
            # build.user = request.user
            # build.hero = build.hero

            form.save()

            messages.success(request, 'Build has been updated.')
            # return redirect(home)
            return redirect('builds:view_build', build_id=build_id)

        context = {
            'build': build,
            'gearset_quantity_choices': gearset_quantity_choices,
            'gearset_type_choices': gearset_type_choices,
            'unique_treasure_choices': unique_treasure_choices,
            'unique_treasure_options_choices': unique_treasure_options_choices,
            'gear_options_num_choices': gear_options_num_choices,
            'dropdown_stats_choices': dropdown_stats_choices,
            'enchants_choices': dropdown_stats_choices,
            'runes_weapons_choices': runes_weapons_choices,
            'runes_armor_choices': runes_armor_choices,
            'runes_secondary_choices': runes_secondary_choices,
            'accessory_type_choices': accessory_type_choices,
            # 'runes_velkazar_choices': runes_velkazar_choices,
            'runes_velkazar_95_choices': runes_velkazar_95_choices,
            'unique_treasure_num_choices': unique_treasure_num_choices,
            'runes_weapon1_velk': runes_weapon1_velk,
            'runes_weapon2_velk': runes_weapon2_velk,
            'runes_weapon3_velk': runes_weapon3_velk,
            'runes_armor_velk': runes_armor_velk,
            'runes_secondary_velk': runes_secondary_velk,
            'main_content': main_content,
            'sub_content': sub_content,
        }
        return render(request, 'builds/edit_build.html', context)

    else:
        return redirect(home)

@login_required
def delete_build(request, build_id):
    build = Build.objects.get(id=build_id)
    if request.user == build.user:
        if request.method == 'POST':
            # build = Build.objects.get(id=build_id)
            build.delete()
            return redirect('dashboard')
        else:
            return redirect('dashboard')
    else:
        return redirect('home')

@login_required(login_url='/accounts/login')
def select_hero(request):
    
    heroes = Hero.objects.all().order_by('hero_name')
    context = {
            'heroes': heroes,
            
        }
    return render(request, 'builds/create_build/select_hero.html', context)

@login_required(login_url='/accounts/login')
def create_build(request, hero_name):
    hero = Hero.objects.get(hero_name=hero_name)
    main_content = Content_Tag.objects.distinct('main_content')
    sub_content = Content_Tag.objects.all().order_by('sub_content')
    # artifacts = Artifact.objects.all()

    if request.method == 'POST': 
        form = BuildForm(request.POST or None)

        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            build = form.save(commit=False)
            build.user = request.user
            build.hero = hero
            
            build.save()

            messages.success(request, 'Build created successfully.')

            # redirect to a new URL:
            return redirect('home')
        else:
            # form = BuildForm() 
            context = {
                'form': form,
                'hero': hero,
                'accessory_type_choices': accessory_type_choices,
                'dropdown_stats_choices': dropdown_stats_choices,
                'runes_velkazar_95_choices': runes_velkazar_95_choices,
                'unique_treasure_num_choices': unique_treasure_num_choices,
                'main_content': main_content,
                'sub_content': sub_content,
            }

            return render(request, 'builds/create_build.html', context)

    else: 
        form = BuildForm()

    context = {
        'form': form,
        'hero': hero,
        'accessory_type_choices': accessory_type_choices,
        'dropdown_stats_choices': dropdown_stats_choices,
        'runes_velkazar_95_choices': runes_velkazar_95_choices,
        'unique_treasure_num_choices': unique_treasure_num_choices,
        'main_content': main_content,
        'sub_content': sub_content,
    }
    return render(request, 'builds/create_build.html', context)
        
def privacy_policy(request):
    return render(request, 'footer/privacy_policy.html')

def terms_of_service(request):
    return render(request, 'footer/terms_of_service.html')

def about(request):
    return render(request, 'footer/about.html')

def faqs(request):
    return render(request, 'footer/faqs.html')