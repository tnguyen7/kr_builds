# Generated by Django 2.2.3 on 2019-09-09 01:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('builds', '0008_auto_20190901_2251'),
    ]

    operations = [
        migrations.AddField(
            model_name='hero',
            name='unique_treasure1_image',
            field=models.ImageField(default='def', upload_to='unique_treasures'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='hero',
            name='unique_treasure2_image',
            field=models.ImageField(default='def', upload_to='unique_treasures'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='hero',
            name='unique_treasure3_image',
            field=models.ImageField(default='def', upload_to='unique_treasures'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='hero',
            name='unique_treasure4_image',
            field=models.ImageField(default='def', upload_to='unique_treasures'),
            preserve_default=False,
        ),
    ]
