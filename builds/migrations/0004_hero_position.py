# Generated by Django 2.2.3 on 2019-08-10 01:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('builds', '0003_hero_atk_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='hero',
            name='position',
            field=models.CharField(choices=[('Front', 'Front'), ('Middle', 'Middle'), ('Back', 'Back')], default='Front', max_length=25),
            preserve_default=False,
        ),
    ]
