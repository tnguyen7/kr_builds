# Generated by Django 2.2.3 on 2019-10-01 01:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('builds', '0021_skills'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Skills',
            new_name='Skillset',
        ),
    ]
