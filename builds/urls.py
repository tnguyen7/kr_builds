from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'builds'

urlpatterns = [
    
    path('select_hero/', views.select_hero, name='select_hero'),
    path('create_build/<str:hero_name>', views.create_build, name='create_build'),
    path('view_build/<int:build_id>', views.view_build, name='view_build'),
    path('edit_build/<int:build_id>', views.edit_build, name='edit_build'),
    path('delete_build/<int:build_id>', views.delete_build, name='delete_build'),
    path('upvote/<int:build_id>', views.upvote, name='upvote'),
    path('downvote/<int:build_id>', views.downvote, name='downvote'),
    # path('<int:build_id>/exists', views.exists, name='exists'),
    
    
    

]
    
