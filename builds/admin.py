from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from .models import Build, Hero, Content_Tag, Skillset

admin.site.register(Build)
# admin.site.register(Hero)
# admin.site.register(Content_Tag)

@admin.register(Hero)
class HeroAdmin(ImportExportModelAdmin):
    pass

@admin.register(Skillset)
class SkillsetAdmin(ImportExportModelAdmin):
    pass

@admin.register(Content_Tag)
class Content_TagAdmin(ImportExportModelAdmin):
    pass