from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from builds.models import Build

def register(request):
    if request.method == 'POST':
        if request.POST['password1'] == request.POST['password2']:

            try:
                user = User.objects.get(username=request.POST['username'])
                return render(request, 'accounts/register.html', {'error':'Username has already been taken'}) 
            except User.DoesNotExist:
                user = User.objects.create_user(request.POST['username'], password=request.POST['password1'])
                login(request, user)
                return render(request, 'accounts/register.html') 
        else:
            return render(request, 'accounts/register.html', {'error':'Passwords didn\'t match'}) 
    else:
        return render(request, 'accounts/register.html')    

def user_login(request):
    if request.method == 'POST':
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])

        if user is not None:
            login(request, user)
            if 'next' in request.POST:
                return redirect(request.POST['next'])
            return redirect('home')
        else:
            return render(request, 'accounts/login.html', {'error':'The Username and Password didn\'t match'}) 
    else:
        return render(request, 'accounts/login.html')    

def user_logout(request):
    if request.method == 'POST':
        logout(request)
        return redirect('home')

def dashboard(request):
    user_builds = Build.objects.order_by('-created_at').filter(user_id=request.user.id)

    context = {
        'builds': user_builds
    }

    return render(request, 'accounts/dashboard.html', context)